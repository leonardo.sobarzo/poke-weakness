# Poke-weakness

Simple rust cli program to show which pokemons are weak agains a certain pokemons. This only takes into account the type of the pokemon and is only to practice rust.

## Usage

`./pokeweakness`

## Pokemon Data

The file `pokemons.json` was downloaded from [sbishopwta repository](https://github.com/sbishopwta/pokemon-data)
