// Entry point in rust
//use serde::{Seriealize, Deserialize};
//use serde_json;
use std::fs::File;
use std::io::prelude::Read;

struct Poke {
    id: i32,
    poke: Pokemon,
}

struct Pokemon {
    name: String,
    attack: i16,
    defense: i16,
    evolve_level: i16,
    evolve_to: String,
    the_type: String,
    //moves: something,
    curve: f32,
    //levels: array of ints,
    probability: i8
}

fn main() {
    //let pokemons_file = serde_json::parse("src/data/pokemons.json").unwrap();
    let mut pokemon_json = File::open("src/data/pokemons.json").expect("Can't open the json");
    let mut contents = String::new();
    pokemon_json.read_to_string(&mut contents).expect("Can't read file");
    println!("contents?: {}", contents);
}

